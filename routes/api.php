<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Tochka\JsonRpc\JsonRpcServer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/ping', function (Request $request) {
    return ['pong'];
});

Route::middleware('api')->post('/v1/jsonrpc', function (Request $request, JsonRpcServer $server) {
    return $server->handle($request);
});

