<?php

namespace App\Http\Controllers\RPC;

use App\PageData;
use Tochka\JsonRpc\Traits\JsonRpcController;

/**
 * Сохранение и получение данных для страницы.
 */
class PageDataController
{
    use JsonRpcController;

    /**
     * @var PageData\PageDataRepositoryInterface
     */
    private PageData\PageDataRepositoryInterface $repository;

    /**
     * @param PageData\PageDataRepositoryInterface $repository
     */
    public function __construct(PageData\PageDataRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Сохранение данных для страницы.
     *
     * @param string $pageUid Уникальный идентификатор страницы.
     * @param object $data Key-value хранилище данных для страницы.
     * @return int
     *
     * @throws \Tochka\JsonRpc\Exceptions\RPC\InvalidParametersException
     */
    public function store(string $pageUid, object $data): int
    {
        $this->validate([
            'pageUid' => ['required', 'max:255', 'min:2'],
            'data' => ['required', 'filled'],
        ]);

        $this->repository->save($pageUid, $data);

        return true;
    }

    /**
     * Получнеие данных для страницы.
     *
     * @param string $pageUid Уникальный идентификатор страницы.
     * @param int $limit Количество отдаваемых результатов.
     * @param int $offset Позволяет пропустить указанное количество результатов.
     * @return array
     *
     * @throws \Tochka\JsonRpc\Exceptions\RPC\InvalidParametersException
     */
    public function list(string $pageUid, int $limit = 10, int $offset = 0): array
    {
        $this->validate([
            'pageUid' => ['required', 'max:255', 'min:2'],
            'limit' => ['integer', 'gte:1', 'lte:100'],
            'offset' => ['integer', 'gte:0'],
        ]);

        $count = $this->repository->countByPageUid($pageUid);
        $collection = $this->repository->findAllByPageUid($pageUid, $limit, $offset);

        if (count($collection) > 0) {
            return [
                'items' => $collection,
                'total' => $count,
                'limit' => $limit,
                'offset' => $offset,
            ];
        } else {
            return [
                'items' => [],
                'total' => $count,
                'limit' => $limit,
                'offset' => $offset,
            ];
        }
    }
}
