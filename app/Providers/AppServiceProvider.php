<?php

namespace App\Providers;

use App\PageData\PageDataRepositoryInterface;
use App\Repository\PageDataEloquentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PageDataRepositoryInterface::class, PageDataEloquentRepositoryInterface::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
