<?php

namespace App\Repository;

use App\PageData;
use App\PageData\PageDataInterface;
use App\PageData\PageDataRepositoryInterface;

class PageDataEloquentRepositoryInterface implements PageDataRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function save(string $pageUid, object $data): PageDataInterface
    {
        /** @var PageData $model */
        $model = PageData::query()->create([
            'page_uid' => $pageUid,
            'data' => $data,
        ]);
        $model->save();

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function findAllByPageUid(string $pageUid, $limit = 10, $offset = 0): array
    {
        $q = $this->findAllQuery($pageUid);

        return $q->limit($limit)->offset($offset)->get()->toArray();
    }

    /**
     * @inheritDoc
     */
    public function countByPageUid(string $pageUid): int
    {
        $q = $this->findAllQuery($pageUid);

        return $q->count();
    }

    private function findAllQuery(string $pageUid): \Illuminate\Database\Eloquent\Builder
    {
        return PageData::query()
            ->where('page_uid', '=', $pageUid)
            ->orderByDesc('created_at')
        ;
    }
}
