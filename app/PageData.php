<?php

namespace App;

use App\Casts\JsonObject;
use App\PageData\PageDataInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PageData
 * @package App
 *
 * @property int $id
 * @property \DateTimeInterface $created_at
 * @property \DateTimeInterface $updated_at
 * @property string $page_uid
 * @property object $data
 */
class PageData extends Model implements PageDataInterface
{
    /**
     * @inheritDoc
     */
    protected $casts = [
        'data' => JsonObject::class,
    ];

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getPageUid(): string
    {
        return (string) $this->page_uid;
    }

    /**
     * @inheritDoc
     */
    public function getData(): object
    {
        return (object) $this->data;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        if (!$this->created_at instanceof \DateTimeInterface) {
            return new \DateTime;
        }

        return $this->created_at;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        if (!$this->updated_at instanceof \DateTimeInterface) {
            return new \DateTime;
        }

        return $this->updated_at;
    }

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format(DATE_RFC3339);
    }
}
