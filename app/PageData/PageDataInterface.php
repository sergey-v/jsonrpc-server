<?php


namespace App\PageData;

interface PageDataInterface
{
    public function getId(): int;
    public function getPageUid(): string;
    public function getData(): object;
    public function getCreatedAt(): \DateTimeInterface;
    public function getUpdatedAt(): \DateTimeInterface;
}
