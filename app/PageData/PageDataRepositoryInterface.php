<?php

namespace App\PageData;

interface PageDataRepositoryInterface
{
    /**
     * Save PageData.
     *
     * @param string $pageUid
     * @param object $data
     * @return PageDataInterface
     */
    public function save(string $pageUid, object $data): PageDataInterface;

    /**
     * Find all data by pageUid.
     *
     * @param string $pageUid
     * @param int $limit
     * @param int $offset
     * @return array|PageDataInterface[]
     */
    public function findAllByPageUid(string $pageUid, $limit = 10, $offset = 0): array;

    public function countByPageUid(string $pageUid): int;
}
