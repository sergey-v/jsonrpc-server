<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PageData;
use Faker\Generator as Faker;

$factory->define(PageData::class, function (Faker $faker) {
    return [
        'page_uid' => $faker->randomElement([
            'welcome',
            'promo',
            'about',
        ]),
        'data' => (object) [
            'ip' => $faker->ipv4,
            'email' => $faker->unique()->safeEmail,
        ]
    ];
});
