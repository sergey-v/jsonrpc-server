<?php

use Illuminate\Database\Seeder;

class PageDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\PageData::class, 100)->create()->each(function ($data) {
            $data->save();
        });
    }
}
