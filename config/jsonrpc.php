<?php

/**
 * Настройки JsonRpc
 */
return [
    /**
     * Описание сервиса (для SMD-схемы)
     */
    'description' => 'JsonRpc Server',

    /**
     * Пространство имен для контроллеров по умолчанию
     */
    'controllerNamespace' => 'App\\Http\\Controllers\\RPC\\',

    /**
     * Суффикс для имен контроллеров по умолчанию
     */
    'controllerPostfix' => 'Controller',

    /**
     * Контроллер по умолчанию для методов без имени сервиса (ping)
     */
    'defaultController' => 'Api',

    /**
     * Обработчики запросов
     */
    'middleware' => [
        \Tochka\JsonRpc\Middleware\ValidateJsonRpcMiddleware::class,     // валидация на стандарты JsonRPC
        \Tochka\JsonRpc\Middleware\MethodClosureMiddleware::class,       // возвращает контроллер и метод !!REQUIRED!!
        \Tochka\JsonRpc\Middleware\AssociateParamsMiddleware::class,     // ассоциативные параметры
    ],

    /**
     * Настройки логирования
     */
    'log' => [
        /**
         * Канал лога, в который будут записываться все логи
         */
        'channel' => 'jsonrpclog',

        /**
         * Параметры, которые необходимо скрыть из логов
         */
        'hideParams' => []
    ],

    'authValidate' => false,

];
